## Create dataset for practical example
library("dplyr")
library("lubridate")
library("gridExtra")
library("ggplot2")
library("viridis")
# Load SST data -- sst from AVHRR
# load("../Extract_satellite_data/9km_1week/AVHRR_1_1week_9km.Rdata")
load("../Extract_satellite_data/9km_1week/MODISAQUA_37_1week_9km.RData")
getwd()
setwd("../../Practical_example")
# Load SST data -- sst from AVHRR

load("../Extract_satellite_data/9km_1week/MODISAQUA_37_1week_9km.RData")



Desiredtagtype <- c("argos") #or "geolocation"
load("../Project_KStesteverything/Tagtypes.RData")
list_eventids <- A$eventid[A$type %in% Desiredtagtype] #the list of event ids that we want to consider here
TRACKS <- TRACKS[which(TRACKS$event_id %in% list_eventids),] # Here select the kind of tags we want - we select only the event ids that correspond to the tag type we want (argos, geolocation...)


events <- unique(TRACKS$event_id[TRACKS$species_id==16])
TRACKS <- subset(TRACKS, TRACKS$event_id %in% events)
Brownian <- lapply(Brownian, function(DF){subset(DF,DF$event_id %in% events)})
Levy <- lapply(Levy, function(DF){subset(DF,DF$event_id %in% events)})
CRW <- lapply(CRW, function(DF){subset(DF,DF$event_id %in% events)})
JD <- lapply(JD, function(DF){subset(DF,DF$event_id %in% events)})
#Put all the tracks in a single dataframe
Brownian <- bind_rows(Brownian, .id = "column_label") #column_label is the run# for all the synthetic tracks
Levy <- bind_rows(Levy, .id = "column_label")
CRW <- bind_rows(CRW, .id = "column_label")
JD <- bind_rows(JD, .id = "column_label")

## Here plot SST histograms
colors = c("black","red","blue","darkgoldenrod2","purple","chartreuse3")
szl <- 1
q2 <- ggplot(TRACKS, aes(x=WM_sst) ) + scale_x_continuous(limits = c(8,34), expand = c(0,0), breaks = c(10,15,20,25,30)) + geom_density(color = colors[1], alpha = .5, size = szl) +
  geom_density(data = Brownian, aes(x = WM_sst), color = colors[2], alpha = .5, size = szl)+
  geom_density(data = Levy, aes(x = WM_sst), color = colors[3], alpha = .5, size = szl) +
  geom_density(data = CRW, aes(x = WM_sst), color = colors[5], alpha = .5, size = szl) +
  geom_density(data = JD, aes(x = WM_sst), color = colors[6], alpha = .5, size = szl) + theme_bw() + labs(x = "", y = "")  +
  scale_y_continuous(limits = c(0,0.135), breaks = c(0,0.05, 0.1, 0.15, 0.2, 0.3, 0.4), labels = c("0", "0.05","0.1","0.15", "0.2", "0.3", "0.4"), expand = c(0, 0))

pdf(file = "histograms_allblues_argos.pdf", width = 3, height = 5/3, pointsize = 16)
print(q2)
dev.off()
################################################################################
#################### Map plotting ##############################################
################################################################################
library("glue")
i <- 1
# Levy$latitude[(Levy$latitude>(-(Levy$longitude%%360)+275)) & Levy$latitude<32.5] <- NA
# CRW$latitude[(CRW$latitude>(-(CRW$longitude%%360)+275)) & CRW$latitude<32.5] <- NA
# JD$latitude[(JD$latitude>(-(JD$longitude%%360)+275)) & JD$latitude<32.5] <- NA
xlims <- c(200,260) %% 360 #)  c(min(subset(TRACKS, month(TRACKS$date) %in% (3*(i-1)+1):(3*i))$longitude %%360 )-2, max(subset(TRACKS, month(TRACKS$date) %in% (3*(i-1)+1):(3*i))$longitude %%360 )+2)
ylims <- c(-5,55) #,50) #min(subset(TRACKS, month(TRACKS$date) %in% (3*(i-1)+1):(3*i))$latitude)-2, max(subset(TRACKS, month(TRACKS$date) %in% (3*(i-1)+1):(3*i))$latitude)+2)
szp <- .1
assign(paste(glue("T{i}")), ggplot(TRACKS, aes(x = longitude %% 360, y = latitude)) + theme_classic() +
         borders(database = "world2") +  coord_fixed(xlim = xlims,ylim=ylims) +
         geom_point(colour = colors[1], size = szp ))
assign(paste(glue("B{i}")), ggplot(Brownian, aes(x = longitude %% 360, y = latitude)) + theme_classic() +
         borders(database = "world2") +  coord_fixed(xlim = xlims,ylim=ylims) +
         geom_point(colour = colors[2], size = szp ))
assign(paste(glue("L{i}")), ggplot(Levy, aes(x = longitude %% 360, y = latitude)) + theme_classic() +
         borders(database = "world2") +  coord_fixed(xlim = xlims,ylim=ylims) +
         geom_point(colour = colors[3], size = szp ))
assign(paste(glue("C{i}")), ggplot(CRW, aes(x = longitude %% 360, y = latitude)) + theme_classic() +
         borders(database = "world2") +  coord_fixed(xlim = xlims,ylim=ylims) +
         geom_point(colour = colors[5], size = szp ))
assign(paste(glue("J{i}")), ggplot(JD, aes(x = longitude %% 360, y = latitude)) + theme_classic() +
         borders(database = "world2") +  coord_fixed(xlim = xlims,ylim=ylims) +
         geom_point(colour = colors[6], size = szp ))
grid.arrange(T1, B1, L1, C1, J1, nrow = 1)


## Test for all tracks agglomerated
ALT <- "less" # c("less","greater","two.sided")
ks.test(TRACKS$WM_sst,Brownian$WM_sst, alternative = ALT)
ks.test(TRACKS$WM_sst,Levy$WM_sst, alternative = ALT)
ks.test(TRACKS$WM_sst,CRW$WM_sst, alternative = ALT)
ks.test(TRACKS$WM_sst,JD$WM_sst, alternative = ALT)
ks.test(TRACKS$WM_sst,c(JD$WM_sst,CRW$WM_sst,Levy$WM_sst,Brownian$WM_sst), alternative = ALT)
ALT <- "greater" # c("less","greater","two.sided")
ks.test(TRACKS$WM_sst,Brownian$WM_sst, alternative = ALT)
ks.test(TRACKS$WM_sst,Levy$WM_sst, alternative = ALT)
ks.test(TRACKS$WM_sst,CRW$WM_sst, alternative = ALT)
ks.test(TRACKS$WM_sst,JD$WM_sst, alternative = ALT)
ks.test(TRACKS$WM_sst,c(JD$WM_sst,CRW$WM_sst,Levy$WM_sst,Brownian$WM_sst), alternative = ALT)



## Test for each individual track
Colnames <- c("event", "length", "Brownianpval_less", "BrownianD_less", "Brownianpval_greater", "BrownianD_greater",
              "Levypval_less",     "LevyD_less",     "Levypval_greater",     "LevyD_greater",
              "CRWpval_less",     "CRWD_less",      "CRWpval_greater",      "CRWD_greater",
              "JDpval_less",      "JDD_less",       "JDpval_greater",       "JDD_greater",
              "Allpval_less",    "AllD_less",      "Allpval_greater",      "AllD_greater")
Singletests <- setNames(data.frame(matrix(ncol = length(Colnames), nrow = 0)), Colnames)
for (j in 1:length(events)){
  a <- ks.test(TRACKS$WM_sst[TRACKS$event_id==events[j]], Brownian$WM_sst[Brownian$event_id==events[j]], alternative = "less")
  b <- ks.test(TRACKS$WM_sst[TRACKS$event_id==events[j]], Brownian$WM_sst[Brownian$event_id==events[j]], alternative = "greater")
  c <- ks.test(TRACKS$WM_sst[TRACKS$event_id==events[j]], Levy$WM_sst[Levy$event_id==events[j]], alternative = "less")
  d <- ks.test(TRACKS$WM_sst[TRACKS$event_id==events[j]], Levy$WM_sst[Levy$event_id==events[j]], alternative = "greater")
  e <- ks.test(TRACKS$WM_sst[TRACKS$event_id==events[j]], CRW$WM_sst[CRW$event_id==events[j]], alternative = "less")
  f <- ks.test(TRACKS$WM_sst[TRACKS$event_id==events[j]], CRW$WM_sst[CRW$event_id==events[j]], alternative = "greater")
  g <- ks.test(TRACKS$WM_sst[TRACKS$event_id==events[j]], JD$WM_sst[JD$event_id==events[j]], alternative = "less")
  h <- ks.test(TRACKS$WM_sst[TRACKS$event_id==events[j]], JD$WM_sst[JD$event_id==events[j]], alternative = "greater")
  l <- ks.test(TRACKS$WM_sst[TRACKS$event_id==events[j]], c(Brownian$WM_sst[Brownian$event_id==events[j]],  Levy$WM_sst[Levy$event_id==events[j]], CRW$WM_sst[CRW$event_id==events[j]], JD$WM_sst[JD$event_id==events[j]]), alternative = "less")
  m <- ks.test(TRACKS$WM_sst[TRACKS$event_id==events[j]], c(Brownian$WM_sst[Brownian$event_id==events[j]],  Levy$WM_sst[Levy$event_id==events[j]], CRW$WM_sst[CRW$event_id==events[j]], JD$WM_sst[JD$event_id==events[j]]), alternative = "greater")
  Singletests[j,] <- c(events[j], dim(TRACKS[TRACKS$event_id==events[j],])[1],
                       as.numeric(a$p.value), as.numeric(a$statistic), as.numeric(b$p.value), as.numeric(b$statistic),
                       as.numeric(c$p.value), as.numeric(c$statistic), as.numeric(d$p.value), as.numeric(d$statistic),
                       as.numeric(e$p.value), as.numeric(e$statistic), as.numeric(f$p.value), as.numeric(f$statistic),
                       as.numeric(g$p.value), as.numeric(g$statistic), as.numeric(h$p.value), as.numeric(h$statistic),
                       as.numeric(l$p.value), as.numeric(l$statistic), as.numeric(m$p.value), as.numeric(m$statistic))
}

nosel   <- sum(Singletests$Allpval_less>0.05 & Singletests$Allpval_greater>0.05) / dim(Singletests)[1]
lowerT  <- sum(Singletests$Allpval_less>0.05 & Singletests$Allpval_greater<0.05) / dim(Singletests)[1]
higherT <- sum(Singletests$Allpval_less<0.05 & Singletests$Allpval_greater>0.05) / dim(Singletests)[1]
bothT   <- sum(Singletests$Allpval_less<0.05 & Singletests$Allpval_greater<0.05) / dim(Singletests)[1]




pdf(file = "Tests_track_per_track_argos.pdf", width = 3, height = 5/3, pointsize = 16)
ggplot(DF, aes(x = x, y = y)) + geom_col() + theme_bw()+theme(axis.text.x = element_text(angle = 45, hjust = 1))
dev.off()

##Now we bootstrap these estimates
Singletests$signiless <- Singletests$Allpval_less<0.05
Singletests$signigreater <- Singletests$Allpval_greater<0.05

library("boot")

f1 <- function(DF, i){
  d2 <- DF[i,]
  return(sum(d2$signiless)/dim(d2)[1])
}
BS1 <- boot(Singletests, f1, R = 10000)

f2 <- function(DF, i){
  d2 <- DF[i,]
  return(sum(d2$signigreater)/dim(d2)[1])
}
BS2 <- boot(Singletests, f2, R = 10000)

f3 <- function(DF, i){
  d2 <- DF[i,]
  return(sum((d2$signigreater==FALSE) & (d2$signiless==FALSE) )/dim(d2)[1])
}
BS3 <- boot(Singletests, f3, R = 10000)


# DF <- data.frame(y = c(mean(BS3$t),mean(BS1$t),mean(BS2$t),0), x = c("None significant", "Less significant", "Greater significant", "Both significant"),
#                 sd = c(sd(BS3$t), sd(BS1$t), sd(BS2$t), 0) )



ggplot(DF, aes(x = x, y = y)) + geom_col(alpha = 0.6) +
  geom_errorbar(aes(ymin = (y-3*sd), ymax = (y+3*sd), width = 0.2)) + theme_bw()+theme(axis.text.x = element_text(angle = 45, hjust = 1)) + 
  scale_y_continuous(expand = c(0.05,0)) + coord_cartesian(clip = 'off')





################################################################################
Results <- data.frame(frac = c(nosel, lowerT,higherT, bothT), Tests = c("No selection", "Lower T", "higher T", "both"))
indiv <- ggplot(Results, aes(x = Tests, y = frac)) + geom_col() + theme_bw() + labs(x = "", y = "") 
print(indiv)

##  Test every two months

Colnames <- c("event", "length", "Brownianpval_less", "BrownianD_less", "Brownianpval_greater", "BrownianD_greater",
              "Levypval_less",     "LevyD_less",     "Levypval_greater",     "LevyD_greater",
              "CRWpval_less",     "CRWD_less",      "CRWpval_greater",      "CRWD_greater",
              "JDpval_less",      "JDD_less",       "JDpval_greater",       "JDD_greater",
              "Allpval_less",    "AllD_less",      "Allpval_greater",      "AllD_greater")
Every2months <- setNames(data.frame(matrix(ncol = length(Colnames), nrow = 0)), Colnames)

event_2months <- c("Jan-Feb","Mar-Apr","May-Jun","Jul-Aug","Sep-Oct","Nov-Dec")


mointerest <- c(1,2)
for (j in 1:6){
  a <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], Brownian$WM_sst[month(Brownian$date) %in% mointerest], alternative = "less")
  b <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], Brownian$WM_sst[month(Brownian$date) %in% mointerest], alternative = "greater")
  c <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest],    Levy$WM_sst[ month(    Levy$date) %in% mointerest], alternative = "less")
  d <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], Levy$WM_sst[    month(    Levy$date) %in% mointerest], alternative = "greater")
  e <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], CRW$WM_sst[     month(     CRW$date) %in% mointerest], alternative = "less")
  f <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], CRW$WM_sst[     month(     CRW$date) %in% mointerest], alternative = "greater")
  g <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], JD$WM_sst[      month(      JD$date) %in% mointerest], alternative = "less")
  h <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], JD$WM_sst[      month(      JD$date) %in% mointerest], alternative = "greater")
  l <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], c(Brownian$WM_sst[month(Brownian$date) %in% mointerest],  Levy$WM_sst[month(Levy$date)%in% mointerest], CRW$WM_sst[month(CRW$date)%in% mointerest], JD$WM_sst[month(JD$date) %in% mointerest]), alternative = "less")
  m <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], c(Brownian$WM_sst[month(Brownian$date) %in% mointerest], Levy$WM_sst[month(Levy$date)%in% mointerest], CRW$WM_sst[month(CRW$date)%in% mointerest], JD$WM_sst[month(JD$date) %in% mointerest]), alternative = "greater")
  Every2months[j,] <- c(event_2months[j], dim(TRACKS[month(TRACKS$date) %in% mointerest,])[1],
                       as.numeric(a$p.value), as.numeric(a$statistic), as.numeric(b$p.value), as.numeric(b$statistic),
                       as.numeric(c$p.value), as.numeric(c$statistic), as.numeric(d$p.value), as.numeric(d$statistic),
                       as.numeric(e$p.value), as.numeric(e$statistic), as.numeric(f$p.value), as.numeric(f$statistic),
                       as.numeric(g$p.value), as.numeric(g$statistic), as.numeric(h$p.value), as.numeric(h$statistic),
                       as.numeric(l$p.value), as.numeric(l$statistic), as.numeric(m$p.value), as.numeric(m$statistic))
  mointerest <- mointerest + 2
}

##  Test every month

Colnames <- c("event", "length", "Brownianpval_less", "BrownianD_less", "Brownianpval_greater", "BrownianD_greater",
              "Levypval_less",     "LevyD_less",     "Levypval_greater",     "LevyD_greater",
              "CRWpval_less",     "CRWD_less",      "CRWpval_greater",      "CRWD_greater",
              "JDpval_less",      "JDD_less",       "JDpval_greater",       "JDD_greater",
              "Allpval_less",    "AllD_less",      "Allpval_greater",      "AllD_greater")
Everymonth <- setNames(data.frame(matrix(ncol = length(Colnames), nrow = 0)), Colnames)

event_2months <- c("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")


mointerest <- c(1)
for (j in 1:12){
  a <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], Brownian$WM_sst[month(Brownian$date) %in% mointerest], alternative = "less")
  b <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], Brownian$WM_sst[month(Brownian$date) %in% mointerest], alternative = "greater")
  c <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest],    Levy$WM_sst[ month(    Levy$date) %in% mointerest], alternative = "less")
  d <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], Levy$WM_sst[    month(    Levy$date) %in% mointerest], alternative = "greater")
  e <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], CRW$WM_sst[     month(     CRW$date) %in% mointerest], alternative = "less")
  f <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], CRW$WM_sst[     month(     CRW$date) %in% mointerest], alternative = "greater")
  g <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], JD$WM_sst[      month(      JD$date) %in% mointerest], alternative = "less")
  h <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], JD$WM_sst[      month(      JD$date) %in% mointerest], alternative = "greater")
  l <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], c(Brownian$WM_sst[month(Brownian$date) %in% mointerest],  Levy$WM_sst[month(Levy$date)%in% mointerest], CRW$WM_sst[month(CRW$date)%in% mointerest], JD$WM_sst[month(JD$date) %in% mointerest]), alternative = "less")
  m <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], c(Brownian$WM_sst[month(Brownian$date) %in% mointerest], Levy$WM_sst[month(Levy$date)%in% mointerest], CRW$WM_sst[month(CRW$date)%in% mointerest], JD$WM_sst[month(JD$date) %in% mointerest]), alternative = "greater")
  Everymonth[j,] <- c(event_2months[j], dim(TRACKS[month(TRACKS$date) %in% mointerest,])[1],
                        as.numeric(a$p.value), as.numeric(a$statistic), as.numeric(b$p.value), as.numeric(b$statistic),
                        as.numeric(c$p.value), as.numeric(c$statistic), as.numeric(d$p.value), as.numeric(d$statistic),
                        as.numeric(e$p.value), as.numeric(e$statistic), as.numeric(f$p.value), as.numeric(f$statistic),
                        as.numeric(g$p.value), as.numeric(g$statistic), as.numeric(h$p.value), as.numeric(h$statistic),
                        as.numeric(l$p.value), as.numeric(l$statistic), as.numeric(m$p.value), as.numeric(m$statistic))
  mointerest <- mointerest + 1
}


##  Test every n month

Colnames <- c("event", "length", "Brownianpval_less", "BrownianD_less", "Brownianpval_greater", "BrownianD_greater",
              "Levypval_less",     "LevyD_less",     "Levypval_greater",     "LevyD_greater",
              "CRWpval_less",     "CRWD_less",      "CRWpval_greater",      "CRWD_greater",
              "JDpval_less",      "JDD_less",       "JDpval_greater",       "JDD_greater",
              "Allpval_less",    "AllD_less",      "Allpval_greater",      "AllD_greater")
Everynmonths <- setNames(data.frame(matrix(ncol = length(Colnames), nrow = 0)), Colnames)

event_nmonths <- c("Jan-Feb-Mar-Apr","May-Jun-Jul-Aug","Sep-Oct-Nov-Dec","xx")

n <- 3
mointerest <- seq(1,n,1)
for (j in 1:(12/n)){
  a <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], Brownian$WM_sst[month(Brownian$date) %in% mointerest], alternative = "less")
  b <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], Brownian$WM_sst[month(Brownian$date) %in% mointerest], alternative = "greater")
  c <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest],    Levy$WM_sst[ month(    Levy$date) %in% mointerest], alternative = "less")
  d <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], Levy$WM_sst[    month(    Levy$date) %in% mointerest], alternative = "greater")
  e <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], CRW$WM_sst[     month(     CRW$date) %in% mointerest], alternative = "less")
  f <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], CRW$WM_sst[     month(     CRW$date) %in% mointerest], alternative = "greater")
  g <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], JD$WM_sst[      month(      JD$date) %in% mointerest], alternative = "less")
  h <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], JD$WM_sst[      month(      JD$date) %in% mointerest], alternative = "greater")
  l <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], c(Brownian$WM_sst[month(Brownian$date) %in% mointerest],  Levy$WM_sst[month(Levy$date)%in% mointerest], CRW$WM_sst[month(CRW$date)%in% mointerest], JD$WM_sst[month(JD$date) %in% mointerest]), alternative = "less")
  m <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% mointerest], c(Brownian$WM_sst[month(Brownian$date) %in% mointerest], Levy$WM_sst[month(Levy$date)%in% mointerest], CRW$WM_sst[month(CRW$date)%in% mointerest], JD$WM_sst[month(JD$date) %in% mointerest]), alternative = "greater")
  Everynmonths[j,] <- c(event_nmonths[j], dim(TRACKS[month(TRACKS$date) %in% mointerest,])[1],
                        as.numeric(a$p.value), as.numeric(a$statistic), as.numeric(b$p.value), as.numeric(b$statistic),
                        as.numeric(c$p.value), as.numeric(c$statistic), as.numeric(d$p.value), as.numeric(d$statistic),
                        as.numeric(e$p.value), as.numeric(e$statistic), as.numeric(f$p.value), as.numeric(f$statistic),
                        as.numeric(g$p.value), as.numeric(g$statistic), as.numeric(h$p.value), as.numeric(h$statistic),
                        as.numeric(l$p.value), as.numeric(l$statistic), as.numeric(m$p.value), as.numeric(m$statistic))
  mointerest <- mointerest + n
}
