######################## Total raster map plot #################################
################################################################################

library("ncdf4")
library("tidyverse")
library("lubridate")
library("gridExtra")
library("ggplot2")
library("viridis")
library("glue")
library("pracma")

xlims <- c(200,260) #These two are species-specific
ylims <- c(10,62)


## To plot CHL A
colmin <- -1.5
colmax <- 1


    #Load everything
SW <-  nc_open("http://basin.ceoe.udel.edu/thredds/dodsC/SEAWIFSMISSION9KM.nc", write = FALSE)
Latitude <- ncvar_get(SW, varid = "lat", start = 1, count = -1)
Longitude <- ncvar_get(SW, varid = "lon", start = 1, count = -1)
Time <- ncvar_get(SW, varid = "time", start = 1, count = -1)
Time <- as.POSIXct(Time, origin = "1970-01-01", tz = "UTC")


Field <- ncvar_get(SW, varid = "chl_ocx", start = c(1,1,1), count = c(-1,-1,1))
nc_close(SW)

Field <- log10(Field)
Field[Field>colmax] <- colmax
Field[Field<colmin] <- colmin

    # Make a dataset with lats and lons and the values
MG <- meshgrid(Latitude, Longitude)
MGlongs <- c(MG$Y)
MGlats <- c(MG$X)
MGvals <- c(Field)
DF <- data.frame(x = MGlongs, y = MGlats, z = MGvals)

    # And then finally plot
tiff("GWS_chla.tiff", units="cm", width=15, height=9, res=600)
ggplot(DF, aes(x=x %% 360,y=y,fill=z)) + geom_tile()  +  scale_fill_viridis(limits = c(colmin,colmax)) + #borders(database = "world2") +
        coord_fixed(xlim = xlims,ylim=ylims) + theme(legend.position = "bottom")
dev.off()

## TO PLOT SST
colmin <- 10
colmax <- 32

    #Load everything
# AVHRR <-  nc_open("http://basin.ceoe.udel.edu/thredds/dodsC/PATH9KMCOMP.nc", write = FALSE)
AVHRR <- nc_open("composite_sst_modis.nc")

xlims <- c(220,255)
ylims <- c(10,45)

Latitude <- ncvar_get(AVHRR, varid = "latitude", start = 1, count = -1)
Longitude <- ncvar_get(AVHRR, varid = "longitude", start = 1, count = -1)
Time <- ncvar_get(AVHRR, varid = "time", start = 1, count = -1)
Time <- as.POSIXct(Time, origin = "1970-01-01", tz = "UTC")

Field <- ncvar_get(AVHRR, varid = "sst", start = c(1,1,1), count = c(-1,-1,1))
nc_close(AVHRR)

Field[Field>colmax] <- colmax
Field[Field<colmin] <- colmin

    #Make a dataset with lats and lons and the values
MG <- meshgrid(Latitude, Longitude)
MGlongs <- c(MG$Y)
MGlats <- c(MG$X)
MGvals <- c(Field)
DF <- data.frame(x = MGlongs, y = MGlats, z = MGvals)

    #And then finally plot
tiff("SST2.tiff", units="cm", width=15, height=9, res=600)
ggplot(DF, aes(x=x %% 360,y=y,fill=z)) + geom_tile()  +  scale_fill_viridis(limits = c(colmin,colmax)) + #borders(database = "world2") +
  coord_fixed(xlim = xlims,ylim=ylims) + theme(legend.position = "bottom")
dev.off()



################################################################################
######################### Plot maps of distribution ############################

colors = c("black","red","blue","darkgoldenrod2","purple","chartreuse3")

for (i in 1:4){
  
  xlims <- c(180,260)#  c(min(subset(TRACKS, month(TRACKS$date) %in% (3*(i-1)+1):(3*i))$longitude %%360 )-2, max(subset(TRACKS, month(TRACKS$date) %in% (3*(i-1)+1):(3*i))$longitude %%360 )+2) 
  ylims <- c(0, 55) #c(min(subset(TRACKS, month(TRACKS$date) %in% (3*(i-1)+1):(3*i))$latitude)-2, max(subset(TRACKS, month(TRACKS$date) %in% (3*(i-1)+1):(3*i))$latitude)+2)
  
  t <- subset(TRACKS, month(TRACKS$date) %in% (3*(i-1)+1):(3*i))
  b <- subset(Brownian, month(Brownian$date) %in% (3*(i-1)+1):(3*i))
  c <- subset(CRW, month(CRW$date) %in% (3*(i-1)+1):(3*i))
  l <- subset(Levy, month(Levy$date) %in% (3*(i-1)+1):(3*i))
  j <- subset(JD, month(JD$date) %in% (3*(i-1)+1):(3*i))
  h <- subset(Hull, month(Hull$date) %in% (3*(i-1)+1):(3*i))
  # t <- subset(t, !(is.na(WM_chl_ocx)))
  
  
  sampl <- sample(1:dim(b)[1], 2000, replace=F)
  b <- b[sampl,]
  l <- l[sampl,]
  c <- c[sampl,]
  j <- j[sampl,]
  h <- h[sampl,]
  
  
  assign(paste(glue("p{i}")), ggplot(t, aes(x = longitude %% 360, y = latitude, color = as.character(species_id))) + theme_classic() +
           borders(database = "world2", size = .1) +  coord_fixed(xlim = xlims,ylim=ylims)  +
           geom_point(shape = 20, size = .2, alpha = 0.5) + scale_color_manual(values = colors[1])  + theme(legend.position = "none",  axis.text=element_blank(),
                                                                                                           axis.ticks=element_blank(), axis.title = element_blank()))#+ scale_color_viridis(limits = c(colmin,colmax)))
  
  assign(paste(glue("p{4+i}")), ggplot(b, aes(x = longitude %% 360, y = latitude, color = as.character(species_id))) + theme_classic() +
           borders(database = "world2", size = .1) +  coord_fixed(xlim = xlims,ylim=ylims) +
           geom_point(shape = 20, size = .2, alpha = 0.5) + scale_color_manual(values = colors[2])  + theme(legend.position = "none",  axis.text=element_blank(),
                                                                                                           axis.ticks=element_blank(), axis.title = element_blank()) )#+ scale_color_viridis(limits = c(colmin,colmax)))
  
  assign(paste(glue("p{8+i}")), ggplot(l, aes(x = longitude %% 360, y = latitude, color = as.character(species_id))) + theme_classic() +
           borders(database = "world2", size = .1) +  coord_fixed(xlim = xlims,ylim=ylims) +  
           geom_point(shape = 20, size = .2, alpha = 0.5) + scale_color_manual(values = colors[3])  + theme(legend.position = "none",  axis.text=element_blank(),
                                                                                                           axis.ticks=element_blank(), axis.title = element_blank()) )#+ scale_color_viridis(limits = c(colmin,colmax)))
  
  assign(paste(glue("p{12+i}")), ggplot(c, aes(x = longitude %% 360, y = latitude, color = as.character(species_id))) + theme_classic() +
           borders(database = "world2", size = .1) +  coord_fixed(xlim = xlims,ylim=ylims) + 
           geom_point(shape = 20, size = .2, alpha = 0.5) + scale_color_manual(values = colors[4])  + theme(legend.position = "none",  axis.text=element_blank(),
                                                                                                           axis.ticks=element_blank(), axis.title = element_blank()) )#+ scale_color_viridis(limits = c(colmin,colmax)))
  
  assign(paste(glue("p{16+i}")), ggplot(j, aes(x = longitude %% 360, y = latitude, color = as.character(species_id))) + theme_classic() +
           borders(database = "world2", size = .1) +  coord_fixed(xlim = xlims,ylim=ylims) +
           geom_point(shape = 20, size = .2, alpha = 0.5) + scale_color_manual(values = colors[5])  + theme(legend.position = "none",  axis.text=element_blank(),
                                                                                                           axis.ticks=element_blank(), axis.title = element_blank()) )#+ scale_color_viridis(limits = c(colmin,colmax)))
  
  assign(paste(glue("p{20+i}")), ggplot(h, aes(x = longitude %% 360, y = latitude, color = as.character(species_id))) + theme_classic() +
           borders(database = "world2", size = .1) +  coord_fixed(xlim = xlims,ylim=ylims) + 
           geom_point(shape = 20, size = .2, alpha = 0.5) + scale_color_manual(values = c(colors[6],colors[6]))  + theme(legend.position = "none",  axis.text=element_blank(),
                                                                                                                         axis.ticks=element_blank(), axis.title = element_blank()) )#+ scale_color_viridis(limits = c(colmin,colmax)))
}

tiff("distrib_blue.tiff", units="cm", width=15, height=9, res=600)
grid.arrange(p1,p5,p9,p13,p17,p21,
             p2,p6,p10,p14,p18,p22,
             p3,p7,p11,p15,p19,p23,
             p4,p8,p12,p16,p20,p24, nrow = 4, ncol = 6)
dev.off()

             
             # p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,
             # p16,p17,p18,p19,p20,p21,p22,p23,p24,nrow=6, ncol=4)
