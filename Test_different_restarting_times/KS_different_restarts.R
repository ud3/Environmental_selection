library(lubridate)
## KS tests for different restarts
ALT = "less"
k <- ks.test(TRACKS$WM_sst,BR_norestart$WM_sst, alternative = ALT)
D <- k$statistic
p <- k$p.value


#KS tests per month
q <- c(10,11,12)
for (q in 7:12){  
  ALT = "less"
  q <- c(11,12)
k <- ks.test(TRACKS$WM_sst[month(TRACKS$date) %in% q],BR_norestart$WM_sst[month(BR_norestart$date) %in% q], alternative = ALT)
D <- k$statistic
p <- k$p.value
print(c(D,p))

}
